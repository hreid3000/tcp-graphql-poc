import produce from 'immer';

import {
  FETCH_POSTS_SUCCEEDED,
  FETCH_POST_SUCCEEDED
} from "store/posts/actions";

const defaultState = {
  list: [],
  currentPost: {},
};

export default (state = defaultState, { type, payload, meta }) => {
  /* eslint-disable no-param-reassign */
  const newState = produce(state, draft => {
    switch (type) {
      case FETCH_POSTS_SUCCEEDED:
        draft.list = payload;
        break;
      case FETCH_POST_SUCCEEDED:
        draft.currentPost = payload;
        break;
      default:
    }
  });
  return newState;
  /* eslint-enable no-param-reassign */
}