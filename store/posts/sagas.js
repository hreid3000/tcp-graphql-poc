import { takeLatest, fork, call, put } from "redux-saga/effects";
import apolloClient from 'lib/apolloClient';
import gql from "graphql-tag";

// import api from "services/api";
import {
  FETCH_POSTS,
  FETCH_POSTS_SUCCEEDED,
  FETCH_POST,
  FETCH_POST_SUCCEEDED
} from "store/posts/actions";

function* fetchPosts(action) {
  const getAllPosts = `
    query test { 
      getAllPosts { id, author, title, content }
    }
  `;

  const posts = yield call(apolloClient.query, {
    query: gql(getAllPosts),
  });
  yield put({ type: FETCH_POSTS_SUCCEEDED, payload: posts.data.getAllPosts });
}

function* fetchPost({ payload }) {
  const getPost = `
  query getPost {
    getPost(id: "${payload}") {
      id, author, body: content, title
    }
  }
  `;
  const post = yield call(apolloClient.query, {
    query: gql(getPost),
  });
  yield put({ type: FETCH_POST_SUCCEEDED, payload: post.data.getPost });
}

function* watchFetchPosts() {
  yield takeLatest(FETCH_POSTS, fetchPosts);
}

function* watchFetchPost() {
  yield takeLatest(FETCH_POST, fetchPost);
}

export default function* postsSagas() {
  yield fork(watchFetchPosts);
  yield fork(watchFetchPost);
}
