vcl 4.0;

import std;

backend node {
  .host = "docker.for.mac.localhost";
  .port = "3000";
}

sub vcl_backend_response {
//  # Enable ESI support
//  if (beresp.http.Surrogate-Control ~ "ESI/1.0") {
//    unset beresp.http.Surrogate-Control;
//    set beresp.do_esi = true;
//  }
  set beresp.ttl = 600s;
}

sub vcl_recv {
  # Remove cookies to prevent a cache miss, you maybe don't want to do this!
  unset req.http.Cookie;

  # Announce ESI support to Node (optional)
//  set req.http.Surrogate-Capability = "key=ESI/1.0";
}

# Set a header to track a cache HIT/MISS.
sub vcl_deliver {
  std.syslog(180, "DELIVER: " + req.url);
  if (obj.hits > 0) {
    set resp.http.X-Varnish-Cache = "HITS=(" + obj.hits + "); ";
  } else {
    set resp.http.X-Varnish-Cache = "MISS";
  }
  return(deliver);
}