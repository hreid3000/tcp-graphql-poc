FROM cooptilleuls/varnish:6.0-alpine AS varnish

COPY docker/varnish/default.vcl /usr/local/etc/varnish/default.vcl