import { Component } from "react";
import { bool } from "prop-types";

class BasePageContainer extends Component {

  constructor (props) {
    super(props);
  }

  static propTypes = {
    initialized: bool
  };

  static defaultProps = {
    initialized: true
  };

  /**
   * To load deltas, we must use the following approach, I guess, we can
   * add this to a AbstractPageComponent super class
   *
   * @param nextProps
   * @param nextState
   * @param nextContext
   */
  componentWillUpdate (nextProps, nextState, nextContext) {
    if (!this.props.initialized && nextProps.initialized) {
      this.refreshInitialDataAfterHydrate(); // Will fetch details.
    }
  }

  /**
   * Subclasses should override this method
   */
  refreshInitialDataAfterHydrate = () => {};
}

export default BasePageContainer;

