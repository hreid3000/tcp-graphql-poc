import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
import awsConfig from './aws-exports';

const client = new AWSAppSyncClient({
  url: awsConfig.aws_appsync_graphqlEndpoint,
  region: awsConfig.aws_appsync_region,
  disableOffline: true,// TODO:  Use a different storage strategy...typeof window === 'undefined', // We need to disable offline mode during SSR.
  auth: {
    type: AUTH_TYPE.API_KEY,
    apiKey: awsConfig.aws_appsync_apiKey,
  }
});
export default client;